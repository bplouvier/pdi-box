mkdir -p /opt/pentaho
cd /opt/pentaho
wget http://downloads.sourceforge.net/project/pentaho/Data%20Integration/4.4.0-stable/pdi-ce-4.4.0-stable.zip
unzip pdi-ce-4.4.0-stable.zip
rm pdi-ce-4.4.0-stable.zip

# Create spoon launcher
echo Creating spoon launcher
mkdir -p /home/vagrant/.local/share/applications
cat <<EOF > /home/vagrant/.local/share/applications/spoon.desktop
[Desktop Entry]
Version=1.0
Name=Kettle
Comment=Pentaho data integration (Kettle)
GenericName=Web Browser
Keywords=ETL;Kettle;PDI;Spoon
Exec=bash -c "cd /opt/pentaho/data-integration && ./spoon.sh"
Terminal=false
X-MultipleArgs=false
Type=Application
Icon=/opt/pentaho/data-integration/spoon.ico
Categories=
StartupNotify=true
EOF
chown vagrant:vagrant /home/vagrant/.local/share/applications/spoon.desktop

# Download and install MySQL driver
wget http://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.27.tar.gz
tar xfz mysql-connector-java-5.1.27.tar.gz
cp mysql-connector-java-5.1.27/mysql-connector-java-5.1.27-bin.jar /opt/pentaho/data-integration/libext/JDBC/
rm -rf mysql-connector-java-5.1.27*

# Set correct ownership and permission on /opt/pentaho
chown -R vagrant:vagrant /opt/pentaho
chmod +x /opt/pentaho/data-integration/*sh
