# PDI development box #

This project contains a [Veewee](https://github.com/jedi4ever/veewee) 
definition for a [PDI](http://community.pentaho.com/) development box. 
The resulting box (virtual machine) can be used with 
[Vagrant](http://www.vagrantup.com/) to boot a virtual machine containing 
all prerequisits for PDI development.

#### Overview ####

** Definition Name:**
`pdi`

**Box Objective:**
The aim is to setup a vanilla `Ubuntu 32-bit (i386) desktop` with a complete
development environment for Pentaho Data Integration

This definition was based on the template `Ubuntu-12.04.3-desktop-i386`

#### Installation ####
After installing veewee:

    cd path/to/veewee
    git clone https://bplouvier@bitbucket.org/bplouvier/pdi-box.git definitions/pdi    
    veewee vbox build 'pdi'
    veewee vbox export 'pdi'

To use this box in Vagrant:

    vagrant box add 'pdi' 'pdi.box'
    cd path/to/project
    vagrant init 'pdi'
    # edit config to make sure the vm start with gui (gui=true)
    vagrant up

#### System Details ####

* **Version:**                  Ubuntu 12.04.3 Desktop
* **Locale:**                   en_US
* **Keyboard layout:**          US
* **Timezone:**                 UTC
* **Extra Language Support:**   none
* **Machine:**                  1.9GB RAM, 30GB HD
* **VirtualBox Config:**        IO_APIC ON, UTC time, 3D acceleration,
                                Shared clipboard.
* **Deviations from a vanilla Ubuntu Desktop:**
    * Add `vagrant` user and add to `admin group`, disable password
     requirement for `admin group` to run `sudo` commands.
    * Install VirtualBox `Guest Additions`
    * Install Git
    * Install MySQL
    * Install Pentaho Data Integration 4.4.0 (Kettle)
